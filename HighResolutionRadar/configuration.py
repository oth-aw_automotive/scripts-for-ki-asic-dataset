config_lrr = { 
          'name': 'lrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 3, 'bandwidth': 212.7e6, 'f0': 76e9, 
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 2, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 2,
          'field_of_view_azimuth': 15, # +- degree         
         }

config_mrr = {
          'name': 'mrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 4, 'bandwidth': 607.7e6, 'f0': 76e9, 
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 2, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 0.7,
          'field_of_view_azimuth': 45, # +- degree
         }

# Attention: the ramp definition not aligned
config_basic2 =  {
          'name': 'basic2', 'n_ramps': 32, 'n_samples': 512, 'n_mmic': 4, 'n_rx': 16, 'n_tx': 8, 'bandwidth': 607.7e6, 'f0': 76e9, 
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 4, 'n_azimuth': 32, 'antenna_distance_elevation': 1, 'antenna_distance_azimuth': 0.5,
          'field_of_view_azimuth': 90 # +- degree         
         }
