import numpy as np


def get_expected_data_length_from_config(config):
    return config['n_ramps'] * config['n_tx'] * config['n_rx'] * config['n_samples']


def radar_data_reshape(data:np.array, config:dict):
    """
    Reshaping the flatten radar data to a 4-dimensional array

    Args:
        data (np.array): flatten np.array() with size of  n_ramps*n_tx*n_rx*n_samples
        config (dict): dictionary of the radar configuration

    Returns:
        np.array(): data array of shape [n_tx, n_rx, n_ramps, n_samples]
    """
    data = np.reshape(data, (config['n_mmic'], config['n_ramps'], config['n_tx'],
                                config['n_rx'] // config['n_mmic'], config['n_samples']))
    data = np.transpose(data, (2, 0, 3, 1, 4))
    shape = data.shape
    data = np.reshape(data, (shape[0], shape[1] * shape[2], shape[3], shape[4]))
    return data


def radar_reorder_antenna(data :np.array, config : dict, custom_index:tuple=None, rotated:bool=False):
    """
        Function reorders the data depending on the physical location of the antenna.
        Input data shape [n_tx, n_rx, n_ramps, n_samples]
        Attention: the tx dimension depend on the ramp scenario and the physical location.
        The function supports only the time-division-multiplexing (TDM) with a predefined order
        as default in the radar configuration.

    Args:
        data (np.array()): array of the data
        config (dict): configuration settings
        custom_index (tuple, optional): tuple of custom antenna mapping (tx, rx) indices. Defaults to None.
        rotated (bool, optional): rotation of the antenna sheet by 180°. Defaults to False.
        zero_padding (bool, optional): _description_. Defaults to True.

    Returns:
        np.array(): reorded data with same shape as input
    """
    if config['name'] == 'lrr':
        # lrr
        tx_index = np.array([2, 1, 0])
        rx_index = np.array([6, 7, 4, 5, 2, 3, 0, 1])
    elif config['name'] == 'mrr':
        # mrr
        tx_index = np.array([1, 2, 0, 3])
        rx_index = np.array([1, 0, 3, 2, 5, 4, 7, 6])
    elif config['name'] == 'basic2':
        # basic2
        tx_index = np.array([2, 0, 3, 1, 5, 7, 4, 6])
        rx_index = np.array([ 6, 7, 4, 5, 9, 8, 11, 10, 13, 12, 15, 14, 2, 3, 0, 1]) 
    else:
        antenna_sheet_name = config['name']
        raise NameError(f'Unsupported configuration name \"{antenna_sheet_name}\".')

    if custom_index is not None:
        tx_index = custom_index[0]
        rx_index = custom_index[1]
        
    if rotated == True:
        tx_index = np.flip(tx_index)
        rx_index = np.flip(rx_index)
        
    data = np.take(data, tx_index, axis=0)
    data = np.take(data, rx_index, axis=1)
    
    return data


def radar_combine_virtual_antennas(data:np.array, config:dict):
    """
    Virtual array are used to increase the angular resolution without increasing the number of real antennas. 
    The virtual tx and rx positions are combined to match physical layout. 
    Attention: higher variation of the physical layout to the default ones affects the virtual array definition. 

    Args:
        data (np.array): input data array of shape [n_tx, n_rx, n_ramps, n_samples]
        config (dict): configuration dictionary

    Returns:
        np.array(): data array of shape [n_elevation, n_azimuth, n_ramps, n_samples]
    """
    
    assert len(np.shape(data)) == 4, 'Input data is not a 4-dimensional array'
    n_tx, n_rx, n_ramps, n_samples = np.shape(data)
    
    pad_width = [[0,0] for i in range(4)]
    pad_width[0][1] = int(np.ceil(n_tx /config['n_elevation']) * config['n_elevation'] - n_tx)
    data = np.pad(data, pad_width, mode='empty')

    data = np.reshape(data, (config['n_elevation'], config['n_azimuth'], n_ramps, n_samples))
    return data


