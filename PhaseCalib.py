import numpy as np
from scipy import constants
from pathlib import Path
from scipy import io
import HighResolutionRadar.radar_signal_processing as sigproc
import HighResolutionRadar.radar_signal_processing as rsp
from HighResolutionRadar.calibration import calculate_phase_calibration
from HighResolutionRadar import visualize_matplotlib as custom_plt
import matplotlib.pyplot as plt
from scipy.io import loadmat, savemat
from datetime import date
from HighResolutionRadar.data_helper import *
from HighResolutionRadar.configuration import *
from asyncio import base_events


config = config_mrr
base_dir = Path(r'DataSet_20230406_114255_Kalibrierung_HRR')
file_data = '[1680774448224]'
board_nr = '46'
radar_path = base_dir / 'Radar_HRR46_vo_mi' / (file_data + ".bin")
#radar_path = base_dir / 'Radar_HRR29_vo_FS' / (file_data + ".bin")
#radar_path = base_dir / 'Radar_HRR42_vo_BFS' / (file_data + ".bin")
camera_path = base_dir / 'Camera_BFS' / (file_data + ".bmp")

print(radar_path)
plt.figure(figsize=(10,5))
plt.imshow(plt.imread(camera_path))
plt.show()
data = np.fromfile(radar_path, dtype=np.int16) / 2**12
data = radar_data_reshape(data, config)
data = radar_reorder_antenna(data, config, rotated=True)

#%matplotlib inline
plt.figure()
plt.title('Time-Domain Signal')
plt.ylabel('Amplitude')
plt.xlabel('Time-Step (Samples)')
for i in range(0, data.shape[-2]):
    plt.plot(data[0,3,i, :])
    
x_range, y_range = rsp.rangeFft(data, config)
x_doppler, y_doppler = rsp.dopplerFft(y_range, config)
data_physical = radar_combine_virtual_antennas(y_doppler, config)
[ax_elevation, ax_azimuth], y_angle = rsp.azimuthFFT(data_physical, config)

y_range_dB = rsp.convPower(y_range)
y_doppler_dB = rsp.convPower(y_doppler)
with np.errstate(divide='ignore', invalid='ignore'):
# prevent error of zero division
    y_angle_dB = rsp.convPower(y_angle)
    
#%matplotlib inline
plt.figure()
plt.grid('on'); plt.xlabel('range (m)'); plt.ylabel('power (dB)'); plt.title('Range-FFT')
for i in range(8):
    plt.plot(x_range, y_range_dB[1, i, 3, :], '--')
plt.plot(x_range, rsp.NciRangeFft(y_range_dB), linewidth=3)
plt.show()

plt.figure()
plt.grid('on'); plt.xlabel('velocity (m/s)'); plt.ylabel('range (m)'); plt.title('Range-Doppler-Map')
plt.contourf(x_doppler, x_range, np.transpose(y_doppler_dB[0,6,:,:]), cmap='viridis')
plt.colorbar()
plt.show()

rdm_cfar = rsp.ca_cfar(rsp.NciRdm(y_doppler_dB), [8, 8, 3, 3], threshold=0.5)
plt.figure()
plt.grid('on'); plt.xlabel('velocity (m/s)'); plt.ylabel('range (m)'); plt.title('CFAR of non-coherent integrated Range-Doppler-Maps')
plt.contourf(x_doppler, x_range, np.transpose(rdm_cfar[:,:]), cmap='viridis')
plt.colorbar()
plt.show()

cfar_mask = np.cumsum(rdm_cfar, axis=0)[-1, :]
cfar_mask[cfar_mask != 0] = 1
cfar_mask.shape

#%matplotlib inline
angle_cfar = rsp.ca_cfar(y_angle_dB[1,:,32,:], [8, 8, 3, 3], threshold=2.25)
fig, ax = plt.subplots(subplot_kw=dict(projection='polar'), figsize=(20,20))
r, theta = np.meshgrid(x_range, np.radians(ax_azimuth))
ax.set_thetamin(np.degrees(np.min(theta))); ax.set_thetamax(np.degrees(np.max(theta)));ax.set_theta_zero_location("N")
ax.set_xlabel('azimuth'); ax.set_ylabel('range'); ax.set_title('Polar Range-Angle-Map')
#cax = ax.contourf(theta, r, y_angle_dB[0,:, 1, :])
cax = ax.contourf(theta, r, angle_cfar)
plt.colorbar(cax, shrink=.5, pad=0.08)
print(y_angle_dB.shape)

#%matplotlib widget
plt.figure()
plt.grid('on'); plt.xlabel('angle (°)'); plt.ylabel('range (m)'); plt.title('Range-Angle-Map')
plt.contourf(ax_azimuth, x_range, np.transpose(y_angle_dB[0,:,2,:]), cmap='viridis')
plt.colorbar()
plt.show()

#Perform Device Calibration
#The phase calibration of each antenna improves the angle estimation of the device. Therefore, keep a target at a known distance (3m) at the same height and with zero angle. Extract the range fft bin index of that target and calculate the phase calibration vector. 
#If CFAR detection does not show anything or too much then us a different threshold setting. 

calibration_vector = calculate_phase_calibration(y_doppler, 16)

y_doppler_calibrated = y_doppler * calibration_vector[:, :, None, None]
[ax_elevation, ax_azimuth], y_angle_calib = rsp.azimuthFFT(y_doppler_calibrated, config)
#with np.errstate(divide='ignore', invalid='ignore'):
# prevent error of zero division
#    y_angle_calib_dB = rsp.convPower(y_angle_calib)
#    plt.figure()
#plt.grid('on'); plt.xlabel('angle (°)'); plt.ylabel('range (m)'); plt.title('Range-Angle-Map')
#plt.contourf(ax_azimuth, x_range, np.transpose(y_angle_calib_dB[0,:,2,:]), cmap='viridis')
#plt.colorbar()
#fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
#r, theta = np.meshgrid(x_range, np.radians(ax_azimuth))
#ax.set_thetamin(np.degrees(np.min(theta))); ax.set_thetamax(np.degrees(np.max(theta)));ax.set_theta_zero_location("N")
#ax.set_xlabel('azimuth'); ax.set_ylabel('range'); ax.set_title('Polar Range-Angle-Map')
#cax = ax.contourf(theta, r, y_angle_calib_dB[0,:,2,:])
#plt.colorbar(cax, shrink=.5, pad=0.08)

# Store the calibration data as .mat file

outfolderpath = r"C:\Users\4609\Downloads\hrr-python-siw_scenario\hrr_calibrationvectors"
outfilename = 'CS523v2_' + board_nr + '_calib_' + config['name'] + '_' + date.today().strftime("%Y%m%d") + '.mat'
outfullpath = outfolderpath + '/' + outfilename
savemat(outfullpath, {'calib_vec': calibration_vector.flatten()})
print('Saved as: ' + outfolderpath + '/' + outfilename)