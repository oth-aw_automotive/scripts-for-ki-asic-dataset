import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.widgets import Slider, Button
from pathlib import Path
from pylab import imread
import numpy as np
from scipy import signal
import HighResolutionRadar.radar_signal_processing as rsp
#from open3d import *
import time
import sys
import open3d as o3d
import open3d.visualization.gui as gui
import numpy

config_mrr = {
          'name': 'mrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 4, 'bandwidth': 607.7e6, 'f0': 76e9,
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 3, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 0.7,
          'field_of_view_azimuth': 45,  # +- degree
         }

config_lrr = {
          'name': 'lrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 3, 'bandwidth': 212.7e6, 'f0': 76e9,
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 2, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 2,
          'field_of_view_azimuth': 15,  # +- degree
         }


class Camera(object):
    def __init__(self, dir, ax, filename):
        self.dir = dir
        self.p = None
        self.plot(ax, filename)

    def load(self, filename):
        return imread(self.dir/f'{filename}.bmp')

    def update(self, filename):
        self.p.set_array(self.load(filename))

    def plot(self, ax, filename):
        self.p = ax.imshow(self.load(filename))

# cloud = read_point_cloud("cloud.ply") # Read the point cloud
# draw_geometries([cloud]) # Visualize the point cloud 
    
class Lidar(object):
    def __init__(self, dir, ax, filename):
        self.dir = dir
        self.vis = None
        self.plot(ax, filename)


    def load(self, filename):
        return o3d.io.read_point_cloud(str(self.dir/f"{filename}.pcd"))

    def update(self, filename):   
        cloud = self.load(filename)
        self.cloud.points = cloud.points
        self.vis.update_geometry(self.cloud)
        #self.vis.get_view_control().change_field_of_view(-10)
        self.vis.poll_events()
        self.vis.update_renderer()
        print('cloud updated')
        #depth = self.vis.capture_depth_float_buffer()
        depth = self.vis.capture_screen_float_buffer()
        imgdata = np.asarray(depth)
        imgdata = numpy.rot90(imgdata, 1)
        #self.img.set_array(imgdata)
        self.img.set_array(imgdata[200:500, 300:600])
      
        
        
    def plot(self, ax, filename):
        o3d.utility.set_verbosity_level(o3d.utility.VerbosityLevel.Debug)
        self.vis = o3d.visualization.Visualizer()
        self.vis.create_window(window_name='Open3D', width=1000, height=1000, left=50, top=50, visible=True)
        self.cloud = self.load(filename)
        self.vis.add_geometry(self.cloud)
        self.vis.run()
        #rendereroption = o3d.visualization.RenderOption()
        #rendereroption.save_to_json('rendereroptions.json')
       
        #depth = self.vis.capture_depth_float_buffer()
        depth = self.vis.capture_screen_float_buffer()
        imgdata = np.asarray(depth)
        imgdata = numpy.rot90(imgdata, 1)
        #self.img = ax.imshow(imgdata)
        self.img = ax.imshow(imgdata[200:500, 300:600])
        ax.axis('off')
 
        print('plotted')

class Radar(object):
    def __init__(self, dir, ax, filename, config):
        self.dir = dir
        self.config = config
        self.p_time, self.p_rfft, self.p_stft = None , None, None
        self.p_rfft_3d = [[[[None] for _ in range(self.config['n_ramps'])]
            for _ in range(self.config['n_rx'])] for _ in range(self.config['n_tx'])]
        self.plot(ax, filename)
        self.i_tx, self.i_rx, self.i_rmp = 0, 0, 0
    def load(self, filename):
        # time_domain_data, rang_data, stft_data
        return load_radar_data(self.dir/f'{filename}.bin', self.config)

    def update(self, filename, i_tx, i_rx, i_rmp):
        time_data, range_data, stft_data = self.load(filename)
        
        self.i_tx = i_tx
        self.i_rx = i_rx
        self.i_rmp = i_rmp
        #self.p_time.set_ydata(time_data[1])
        self.p_time.set_ydata(time_data[self.i_tx, self.i_rx, self.i_rmp, :])
                    
        #self.p_rfft.set_ydata(range_data[1])
        self.p_rfft.set_ydata(range_data[self.i_tx, self.i_rx, self.i_rmp, :])
        
        stft_data = signal.stft(time_data[self.i_tx, self.i_rx, self.i_rmp, :], fs=20e6, nperseg=20)                   
        self.p_stft.set_array(np.abs(stft_data[-1]))

    def plot(self, ax, filename):
        time_data, range_data, stft_data = self.load(filename)
        
        self.i_tx = 0
        self.i_rx = 0
        self.i_rmp = 0
        
        #self.p_time, = ax[0].plot(time_data[0], time_data[1])
        time_data_x = np.arange(0, time_data.shape[3])
        self.p_time, = ax[0].plot(time_data_x, time_data[self.i_tx, self.i_rx, self.i_rmp, :])
        ax[0].set_title('Time-Data'); ax[0].set_xlabel('sample-number'); ax[0].set_ylabel('amplitude')

        #self.p_rfft, = ax[1].plot(range_data[0], range_data[1])
        range_data_x = np.arange(0, range_data.shape[3])
        self.p_rfft, = ax[1].plot(range_data_x, range_data[self.i_tx, self.i_rx, self.i_rmp, :])
        ax[1].set_title('Range-FFT'); ax[1].set_xlabel('frequency (Hz)'); ax[1].set_ylabel('magnitude (dB)')

        self.p_stft = ax[2].pcolormesh(stft_data[1], stft_data[0], np.abs(stft_data[2]), shading='gouraud')
        ax[2].set_title('STFT'); ax[2].set_xlabel('time (s)'); ax[2].set_ylabel('frequency (Hz)')
        
    def plot_all(self, ax, filename):
        time_data, range_data, stft_data = self.load(filename)
        range_data_x = np.arange(0, range_data.shape[3])     
        for i_tx in range(range_data.shape[0]):
            for i_rx in range(range_data.shape[1]):
                for i_rmp in range(range_data.shape[2]):
                    self.p_rfft_3d[i_tx][i_rx][i_rmp], = ax[1].plot(range_data_x, range_data[i_tx, i_rx, i_rmp, :])

        ax[1].set_title('Range-FFT'); ax[1].set_xlabel('frequency (Hz)'); ax[1].set_ylabel('magnitude (dB)')
        
def load_radar_data_from_binary(file, config):
    data = np.fromfile(file, dtype=np.int16) / 2**12
    data = np.reshape(data, (config['n_mmic'], config['n_tx'] * config['n_rx'] // config['n_mmic'], config['n_ramps'], config['n_samples']))
    data = np.moveaxis(data, 2, 1).reshape((config['n_mmic'], config['n_tx'], config['n_rx'] // config['n_mmic'], config['n_ramps'], config['n_samples']))
    data = np.moveaxis(data, 1, 0).reshape((config['n_tx'], config['n_rx'], config['n_ramps'], config['n_samples']))
    return data


def load_radar_data(file, config, idx_tx=0, idx_rx=0, idx_ramps=0):
    time_data = load_radar_data_from_binary(file, config)
    stft_data = signal.stft(time_data[idx_tx, idx_rx, idx_ramps, :], fs=20e6, nperseg=20)
    x_range, rfft = rsp.rangeFft(time_data, config)
    pfft = 20 * np.log10(np.abs(rfft))
    fft_data = pfft[:,:,:,:]
    pfft = rsp.NciRangeFft(pfft)
    #fft_data = [x_range, pfft]
    time_data_reduced = time_data[idx_tx, idx_rx, idx_ramps, :]
    #time_data = [np.arange(0, len(time_data_reduced)), time_data_reduced]
    time_data = time_data[:,:,:,:]
    return time_data, fft_data, stft_data


if __name__ == "__main__":
    config = config_mrr
    generate_movie = False
    #dir_measurement = Path(r'/media/messwagen/T7/X5_Sensordaten/DataSet_20220824/DataSet_20220824_114256_agressor_lrr_0')
    #dir_measurement = Path(r'/media/messwagen/T7/X5_Sensordaten/DataSet_20220824/DataSet_20220824_103341_ncap2_dynX5_pedestrian_1')
    
    #dir_measurement = Path(r'/media/messwagen/T7/messwagen/sensorsdata/DataSet_20220926_170952_HRR46_lrr_Aggressor')
    #dir_measurement = Path(r'/media/messwagen/T7_1/bmw_x5/sensors_data/20221115_x5/DataSet_20221115_105748_Kalibrierung')
    #dir_measurement = Path(r'/media/messwagen/T7_1/bmw_x5/sensors_data/20221115_x5/DataSet_20221117_145417_Aggressor_HRR36_mrr')
    
    dir_measurement = Path(sys.argv[1])

    filenames = sorted((dir_measurement / 'Radar_HRR46_vo_mi').glob('*.bin'))
    filenames = [x.stem for x in filenames]

    fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(16, 9))
    # Initialize plots 
    filename = filenames[0]
    camera_sls = Camera(dir_measurement / 'Camera_FS120', ax[0, 0], filename)
    camera_cls = Camera(dir_measurement / 'Camera_FS', ax[0, 1], filename)
    camera_crs = Camera(dir_measurement / 'Camera_BFS', ax[0, 2], filename)
    camera_srs = Camera(dir_measurement / 'Camera_BFS120', ax[0, 3], filename)
    radar = Radar(dir_measurement / 'Radar_HRR46_vo_mi', ax[1], filename, config)
    lidar = Lidar(dir_measurement / 'Lidar_OS1_vo', ax[1, 3], filename)

    plt.tight_layout()

    def animate(i):
        filename = filenames[int(i)]
        camera_sls.update(filename)
        camera_cls.update(filename)
        camera_crs.update(filename)
        camera_srs.update(filename)
        radar.update(filename, 0, 0, 0)
        lidar.update(filename)

        return camera_sls, camera_cls, camera_crs, camera_srs, radar, lidar

    def animate2(val):
        filename = filenames[int(sample_slider.val)]
        camera_sls.update(filename)
        camera_cls.update(filename)
        camera_crs.update(filename)
        camera_srs.update(filename)
        radar.update(filename, int(tx_slider.val), int(rx_slider.val), int(rmp_slider.val))
        lidar.update(filename)
        return camera_sls, camera_cls, camera_crs, camera_srs, radar, lidar
    
    def plotcloud(val):
        filename = filenames[int(sample_slider.val)]
        lidar.plot(filename)
        
        
    def surf_frm(b):
        i_frm = sample_slider.val
        while i_frm < len(filenames):
            sample_slider.set_val(i_frm)
            animate2(i_frm)
            plt.pause(0.01)
            i_frm = sample_slider.val +1
        return b
    def surf_tx(b):
        for i_tx in range(config['n_tx']):
            tx_slider.set_val(i_tx);
            filename = filenames[int(sample_slider.val)]
            radar.update(filename, i_tx, int(rx_slider.val), int(rmp_slider.val))
            plt.pause(0.1)
        return radar
    def surf_rx(b):
        for i_rx in range(config['n_rx']):
            rx_slider.set_val(i_rx)
            filename = filenames[int(sample_slider.val)]
            radar.update(filename, int(tx_slider.val), i_rx, int(rmp_slider.val))
            plt.pause(0.1)
        return radar
    def surf_rmp(b):
        i_rmp = rmp_slider.val
        while i_rmp < config['n_ramps']:
            rmp_slider.set_val(i_rmp)
            filename = filenames[int(sample_slider.val)]
            radar.update(filename, int(tx_slider.val), int(rx_slider.val), i_rmp)
            plt.pause(0.01)
            i_rmp = rmp_slider.val +1
        return radar

    def surf_all(b):
        i_frm = sample_slider.val
        while i_frm < len(filenames):
            sample_slider.set_val(i_frm)
            i_tx = 0
            tx_slider.set_val(0)
            i_rx = 0
            rx_slider.set_val(0)
            i_rmp = 0
            rmp_slider.set_val(0)
            animate2(i_frm)
            plt.pause(0.01)
            while i_tx < config['n_tx']:
                tx_slider.set_val(i_tx)
                i_rx = 0
                rx_slider.set_val(0)
                while i_rx < config['n_rx']:
                    rx_slider.set_val(i_rx)
                    i_rmp = 0
                    rmp_slider.set_val(0)
                    while i_rmp < config['n_ramps']:
                        rmp_slider.set_val(i_rmp)
                        i_frm = sample_slider.val
                        i_tx = tx_slider.val
                        i_rx = rx_slider.val
                        i_rmp = rmp_slider.val
                        filename = filenames[i_frm]
                        radar.update(filename, i_tx, i_rx, i_rmp)
                        plt.pause(0.01)
                        i_rmp = rmp_slider.val +1
                    i_rx = rx_slider.val +1
                i_tx = tx_slider.val +1
            i_frm = sample_slider.val +1
        return b        
        
    def plot_all(b):
        filename = filenames[int(sample_slider.val)]
        radar.plot_all(ax[1], filename)
        return radar    
        
    if not generate_movie:
        print('Generate plot with slider')
        # sizes are normalized between 0 and 1
        axfreq = plt.axes([0.20, 0.95, 0.6, 0.03])
        ax_tx = plt.axes([0.20, 0.92, 0.6, 0.03])
        ax_rx = plt.axes([0.20, 0.89, 0.6, 0.03])
        ax_rmp = plt.axes([0.20, 0.86, 0.6, 0.03])
        
        ax_btfrm = plt.axes([0.85, 0.96, 0.045, 0.02])
        ax_btall = plt.axes([0.90, 0.96, 0.045, 0.02])
        ax_bttx = plt.axes([0.85, 0.93, 0.1, 0.02])
        ax_btrx = plt.axes([0.85, 0.90, 0.1, 0.02])
        ax_btrmp = plt.axes([0.85, 0.87, 0.1, 0.02])
        
        ax_btcmb = plt.axes([0.35, 0.6, 0.1, 0.02])
        
        sample_slider = Slider(
            ax=axfreq,
            label='File index',
            valmin=0,
            valmax=len(filenames)-1,
            valinit=0, valfmt="%i",
            valstep=1
        )
        tx_slider = Slider(
            ax=ax_tx,
            label='tx index',
            valmin=0,
            valmax=config['n_tx']-1,
            valinit=0, valfmt="%i",
            valstep=1
        )
        rx_slider = Slider(
            ax=ax_rx,
            label='rx index',
            valmin=0,
            valmax=config['n_rx']-1,
            valinit=0, valfmt="%i",
            valstep=1
        )
        rmp_slider = Slider(
            ax=ax_rmp,
            label='Ramps index',
            valmin=0,
            valmax=config['n_ramps']-1,
            valinit=0, valfmt="%i",
            valstep=1
        )
        
        frm_button = Button(
            ax = ax_btfrm,
            label = 'Surf frames'
        )
        tx_button = Button(
            ax = ax_bttx,
            label = 'Surf tx'
        )
        rx_button = Button(
            ax = ax_btrx,
            label = 'Surf rx'
        )
        rmp_button = Button(
            ax = ax_btrmp,
            label = 'Surf ramps'
        )
        
        all_button = Button(
            ax = ax_btall,
            label = 'Surf all'
        )
        
        cmb_button = Button(
            ax = ax_btcmb,
            label = 'all combinations'
        )

        sample_slider.on_changed(animate2)
        tx_slider.on_changed(animate2)
        rx_slider.on_changed(animate2)
        rmp_slider.on_changed(animate2)
        
        frm_button.on_clicked(surf_frm)
        tx_button.on_clicked(surf_tx)
        rx_button.on_clicked(surf_rx)
        rmp_button.on_clicked(surf_rmp)
        
        all_button.on_clicked(surf_all)
        
        cmb_button.on_clicked(plot_all)
        
        plt.show()
        
 
    else:
        print('Generating a animated plot')
        anim = animation.FuncAnimation(fig, func=animate, frames=len(filenames)-1, interval=20, repeat=False)
        anim.save(f'{dir_measurement}/animated_plots.mp4')




