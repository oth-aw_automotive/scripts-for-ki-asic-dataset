import numpy as np
from scipy.io import loadmat, savemat
import glob, os


def order_antenna_calib_siw_mid():
    nEl, nAz = 3, 16

    ant_distance_az = 0.7
    ant_distance_el = 2

    fov_azimuth = 45  # +- (degree)

    ant = np.array([
        1, 0, 3, 2,  # MMIC C
        5, 4, 7, 6,  # MMIC D
    ])

    idx = []
    idx.append(np.concatenate([ant + 2 * 8, ant + 3 * 8]))  # tx5 - tx6
    idx.append(np.concatenate([ant + 1 * 8, ant + 4 * 8]))  # tx4 - tx7
    idx.append(np.concatenate([ant + 0 * 8]))  # tx3

    return idx, {'nEl': nEl, 'nAz': nAz, 'ant_distance_az': ant_distance_az, 'ant_distance_el': ant_distance_el, 'fov_azimuth': fov_azimuth}


def order_antenna_calib_siw_long():
    nEl, nAz = 2, 16

    ant_distance_az = 2
    ant_distance_el = 2

    fov_azimuth = 15 # +- (degree)

    ant = np.array([
        6, 7, 4, 5,  # MMIC B
        2, 3, 0, 1,  # MMIC A
    ])

    idx = []
    idx.append(np.concatenate([ant + 2 * 8, ant + 1 * 8]))  # tx2 - tx1
    idx.append(np.concatenate([ant + 0 * 8]))  # tx0

    return idx, {'nEl': nEl, 'nAz': nAz, 'ant_distance_az': ant_distance_az, 'ant_distance_el': ant_distance_el, 'fov_azimuth': fov_azimuth}


def order_antenna_calib_basic2():
    nEl, nAz = 4, 32

    ant_distance_az = 0.5
    ant_distance_el = 1

    fov_azimuth = 90  # +- (degree)

    ant = np.array([
        6, 7, 4, 5,  # MMIC B
        9, 8, 11, 10,  # MMIC C
        13, 12, 15, 14,  # MMIC D
        2, 3, 0, 1  # MMIC A
    ])

    idx = []
    idx.append(np.concatenate([ant + 2 * 16, ant + 0 * 16]))  # tx2 - tx0
    idx.append(np.concatenate([ant + 3 * 16, ant + 1 * 16]))  # tx3 - tx1
    idx.append(np.concatenate([ant + 4 * 16, ant + 6 * 16]))  # tx4 - tx6
    idx.append(np.concatenate([ant + 5 * 16, ant + 7 * 16]))  # tx5 - tx7

    return idx, {'nEl': nEl, 'nAz': nAz, 'ant_distance_az': ant_distance_az, 'ant_distance_el': ant_distance_el, 'fov_azimuth': fov_azimuth}


folderpath = 'C:/Users/hillejulian/Downloads/HRR_calibration_data (1)/HRR_calibration_data'
outfolderpath = 'calibration_data'

for file in glob.glob(folderpath+"/*.mat"):
    print(file)

    filename = os.path.basename(file).split('.')[0]

    calib_vec = loadmat(folderpath + '/' + filename + '.mat')["calib_vec"].flatten()

    board_name, _, board_nr,_,_,_,_,date,antenna_sheet =filename.split('_')

    if antenna_sheet == 'basic2':
        idx, settings = order_antenna_calib_basic2()
        antenna_sheet = 'Basic2'
    elif antenna_sheet == 'MRR':
        idx, settings = order_antenna_calib_siw_mid()
        antenna_sheet = 'SiwMid'
    elif antenna_sheet == 'LRR':
        idx, settings = order_antenna_calib_siw_long()
        antenna_sheet = 'SiwLong'
    else:
        raise ValueError(f'ERROR:  <{antenna_sheet}> antenna sheet not supported. '
                             f'Select one of basic2, siw_long_range, siw_mid_range')
    print('antenna_sheet: ' + antenna_sheet)

    outfilename = board_name + '_' + board_nr + '_calib_' + antenna_sheet + '_' + date + '.mat'

    idx = np.concatenate(idx)
    savemat(outfolderpath + '/' + outfilename, {'calib_vec': calib_vec[idx]})

print('done')