import ctypes


def convert_ctypes_struct_to_dict(ctypes_struct):
    result = {}
    for field, _ in ctypes_struct._fields_:
        value = getattr(ctypes_struct, field)
        # if the type is not a primitive and it evaluates to False ...
        if (type(value) not in [int, float, bool]) and not bool(value):
            # it's a null pointer
            value = None
        elif hasattr(value, "_length_") and hasattr(value, "_type_"):
            # Probably an array
            value = list(value)
        elif hasattr(value, "_fields_"):
            # Probably another struct
            value = convert_ctypes_struct_to_dict(value)
        result[field] = value
    return result


def serialize_object(obj, pre_string=''):
    """
    Function receives an objects and converts all attributes into a list of strings.
    Supports ctypes structs, lists, dicts and single elements.
    Use following code to convert the serialized list into a string
    $ string = '\n'.join(serialize_object(obj))
    pre_string is a optional argument that adds the string in front of each element.
    Limitations because it is not recursive build.
    """

    data = []
    for attribute in dir(obj):
        if not attribute.startswith('_'):
            attr = obj.__getattribute__(attribute)

            if isinstance(attr, ctypes.Structure):
                data.append(f'{pre_string}{attribute}: {convert_ctypes_struct_to_dict(attr)}\n')
            elif isinstance(attr, dict):
                tmp = ''
                for key in attr:
                    if isinstance(attr[key], ctypes.Structure):
                        tmp += f'{pre_string}{attribute}_{key}: {convert_ctypes_struct_to_dict(attr[key])} \n'
                    else:
                        tmp += f'{pre_string}{attribute}_{key}: {attr[key]} \n'
                data.append(tmp)
            else:
                data.append(f'{pre_string}{attribute}: {attr}\n')

    return data


