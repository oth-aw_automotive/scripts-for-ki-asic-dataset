import matplotlib
matplotlib.use('TkAgg')
# ['GTK3Agg', 'GTK3Cairo', 'MacOSX', 'nbAgg', 'Qt4Agg', 'Qt4Cairo', 'Qt5Agg', 'Qt5Cairo', 'TkAgg', 'TkCairo', 'WebAgg', 'WX', 'WXAgg', 'WXCairo', 'agg', 'cairo', 'pdf', 'pgf', 'ps', 'svg', 'template']

import numpy as np
import HighResolutionRadar.radar_signal_processing as rsp
from HighResolutionRadar import visualize_matplotlib as custom_plt
from pathlib import Path
from scipy import signal

import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox, Button, RadioButtons


path_rec_folder = Path(r'DataSet_')

config_lrr = {'name': 'lrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 3}
config_mrr = {'name': 'mrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 4}


config_mrr = {
          'name': 'mrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 4, 'bandwidth': 607.7e6, 'f0': 76e9,
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 3, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 0.7,
          'field_of_view_azimuth': 45, # +- degree
         }

config_lrr = {
          'name': 'lrr', 'n_ramps': 64, 'n_samples': 512, 'n_mmic': 2, 'n_rx': 8, 'n_tx': 3, 'bandwidth': 212.7e6, 'f0': 76e9,
          't_ramp': 20.48e-6, 't_jump': 0.6e-6, 't_wait': 6e-6, 't_ramp_delay': 8.64e-6,
          'n_elevation': 2, 'n_azimuth': 16, 'antenna_distance_elevation': 2, 'antenna_distance_azimuth': 2,
          'field_of_view_azimuth': 15, # +- degree
         }

config = config_lrr


class OthData(object):
    def __init__(self, folder, config):

        folder_radar = 'Radar_HRR_vo_mi'
        folder_camera = 'Camera_BFS'
        self.config = config

        path_recording = Path(folder)
        if (path_recording / folder_radar / config['name']).exists():
            path_radar = path_recording / folder_radar / config['name']
        else:
            path_radar = path_recording / folder_radar

        self.files_radar = [x for x in sorted(path_radar.glob('*.bin')) if x.is_file()]
        self.files_camera = [x for x in sorted((path_recording / folder_camera).glob('*.bin')) if x.is_file()]
        self.n_frames = len(self.files_radar)

        files_radar_names = [x.name for x in self.files_radar]
        files_camera_names = [x.name for x in self.files_camera]

    def load_radar_data_from_binary(self, idx_file):
        data = np.fromfile(self.files_radar[idx_file], dtype=np.int16)
        data = np.reshape(data, (self.config['n_mmic'], self.config['n_tx'] * self.config['n_rx'] // self.config['n_mmic'], self.config['n_ramps'], self.config['n_samples']))
        data = np.moveaxis(data, 2, 1).reshape((self.config['n_mmic'], self.config['n_tx'], self.config['n_rx'] // self.config['n_mmic'], self.config['n_ramps'], self.config['n_samples']))
        data = np.moveaxis(data, 1, 0).reshape((self.config['n_tx'], self.config['n_rx'], self.config['n_ramps'], self.config['n_samples']))
        return data

    def load_processed_radar(self, idx_file=0, idx_tx=0, idx_rx=0, idx_ramps=0):
        time_data = self.load_radar_data_from_binary(idx_file)  #[idx_tx, idx_rx, idx_ramps, :]
        f, t, Zxx = signal.stft(time_data[idx_tx, idx_rx, idx_ramps, :], fs=20e6, nperseg=20)
        label = np.zeros_like(time_data[idx_tx, idx_rx, idx_ramps, :])

        x_range, rfft = rsp.rangeFft(time_data, config)
        pfft = 20 * np.log10(np.abs(rfft))
        pfft = rsp.NciRangeFft(pfft)

        info = f'file name: {self.files_radar[idx_file].name}\nindex: tx={idx_tx}, rx={idx_rx}, ramps={idx_ramps},'
        return time_data[idx_tx, idx_rx, idx_ramps, :], [t, f, Zxx], x_range, pfft, label, info


class Application(object):
    def __init__(self, recordings):

        self._index = 194

        self.recordings = recordings
        data, [t, f, Zxx], x_range, pfft,  label, info = self.recordings.load_processed_radar(0)

        self.max_index = self.recordings.n_frames

        fig = plt.figure(figsize=(5.5, 3.5), constrained_layout=True)
        spec = fig.add_gridspec(ncols=2, nrows=1)
        self.fig, self.spec = fig, spec

        grid_spec_figure = spec[0, 0].subgridspec(4, 1, height_ratios=[0.3, 0.1, 0.3, 0.3])
        # Plots
        ax0 = fig.add_subplot(grid_spec_figure[0, 0])
        self.line_data, = ax0.plot(data)
        ax0.set_title('Time-Domain-Signal')
        ax0.set_xlabel('time')
        ax0.set_ylabel('amplitude')

        ax1 = fig.add_subplot(grid_spec_figure[1, 0])
        self.line_label, = ax1.plot(label)
        ax1.set_title('Label')
        ax1.set_xlabel('time')
        ax1.set_ylabel('Class')

        ax2 = fig.add_subplot(grid_spec_figure[2, 0])
        self.line_freq = ax2.pcolormesh(t, f, np.abs(Zxx), shading='gouraud')
        ax2.set_title('Short Term Fourier Transformation')
        ax2.set_xlabel('time')
        ax2.set_ylabel('frequency')

        ax3 = fig.add_subplot(grid_spec_figure[3,0])
        self.line_fft, = ax3.plot(x_range, pfft)
        ax3.set_title('Fourier Transformation')
        ax3.set_xlabel('frequency')
        ax3.set_ylabel('magnitude')

        # ======================
        # Boxes and Buttons
        # ======================
        grid_spec_button = spec[0, 1].subgridspec(10, 2)

        self.info_box = TextBox(fig.add_subplot(grid_spec_button[0, :]), "Info",)
        self.info_box.set_val(info)

        def on_press_key(event):
            if event.key == 'right':
                #inrecrease the index
                self.index += 1
            elif event.key == 'left':
                self.index -= 1

        fig.canvas.mpl_connect('key_press_event', on_press_key)

        plt.show()

    def update(self, index):

        data, [_, _, Zxx], x_range, pfft, label, info = self.recordings.load_processed_radar(index)

        self.line_data.set_ydata(data)
        self.line_label.set_ydata(label)
        self.line_freq.set_array(np.abs(Zxx))
        self.line_fft.set_ydata(pfft)
        self.info_box.set_val(info)

        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        if value <= 0:
            self._index = 0
        elif value >= self.max_index:
            self._index = self.max_index
        else:
            self._index = value

        self.update(self._index)

    @index.deleter
    def index(self):
        del self._index



app = Application(OthData(path_rec_folder, config))






